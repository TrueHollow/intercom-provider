import interComProvider from '../src';
import accountFound from './account-found';
import accountNotFound from './account-notfound';
import { ContractObject, Query } from '../src/Contact';

describe('intercom tests', () => {
  beforeAll(async () => {
    const external_id = accountNotFound.account.activeAccount.owner.id;
    const searchResult: Array<any> = await interComProvider.Contact.search({
      value: external_id,
      operator: '=',
      field: 'external_id',
    } as Query);
    if (searchResult.length) {
      const [existingContract] = searchResult;
      const { id } = existingContract;
      const deleteResult = await interComProvider.Contact.delete(id);
      expect(typeof deleteResult.deleted).toBe('boolean');
    }
  });
  beforeEach(() => {
    jest.setTimeout(30000);
  });
  it('test search (Not found)', async () => {
    const searchResult = await interComProvider.Contact.search({
      value: 'fooBar',
      operator: '=',
      field: 'external_id',
    } as Query);
    expect(searchResult.length).toBe(0);
  });
  it('test search (Found)', async () => {
    const searchResult = await interComProvider.Contact.search({
      value: accountFound.account.activeAccount.owner.id,
      operator: '=',
      field: 'external_id',
    } as Query);
    expect(searchResult.length).toBe(1);
  });
  it('test search and update', async () => {
    const external_id = accountFound.account.activeAccount.owner.id;
    const searchResult = await interComProvider.Contact.search({
      value: external_id,
      operator: '=',
      field: 'external_id',
    } as Query);
    expect(searchResult.length).toBe(1);
    const [existingContact] = searchResult;
    const { id } = existingContact;
    const contractData = {
      role: existingContact.role, // please note, that this field MUST be string!!!
      external_id,
      email: accountFound.account.activeAccount.owner.email,
      name: accountFound.account.activeAccount.displayName,
      custom_attributes: {
        ...existingContact.custom_attributes,
        Role: accountFound.account.activeAccount.roles[0],
        AccountType: accountFound.account.activeAccount.type,
        Vetted: accountFound.account.activeAccount.verified,
        Paid: accountFound.account.activeAccount.paid,
      },
    } as ContractObject;
    const updateResult = await interComProvider.Contact.update(
      id,
      contractData
    );
    expect(typeof updateResult.id).toBe('string');
  });
  it('test search and create new one', async () => {
    const external_id = accountNotFound.account.activeAccount.owner.id;
    const contractData = {
      role: 'user',
      external_id,
      email: accountNotFound.account.activeAccount.owner.email,
      name: accountNotFound.account.activeAccount.displayName,
      custom_attributes: {
        Role: accountFound.account.activeAccount.roles[0],
        AccountType: accountFound.account.activeAccount.type,
        Vetted: accountFound.account.activeAccount.verified,
        Paid: accountFound.account.activeAccount.paid,
      },
    } as ContractObject;
    const createResult = await interComProvider.Contact.create(contractData);
    expect(typeof createResult.id).toBe('string');
  });
});
