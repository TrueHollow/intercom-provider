# README

## Requirements:
1. Node.js 12+

## Settings:
1. InterComProvider require apiToken to make request.
2. App example try to fetch that variable from env or `.env` file.

## Examples:
1. Check tests directory for code examples: `__test__`
