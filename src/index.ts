import InterComProviderOptions from './InterComProviderOptions';
import InterComProvider from './InterComProvider';
import * as path from 'path';
import * as dotenv from 'dotenv';
import * as env from 'env-var';

const envFilePath = path.resolve(__dirname, '..', '.env');
const parseResult = dotenv.config({ path: envFilePath });
if (parseResult.error) {
  if (parseResult.error.name == 'ENOENT') {
    console.log(`env file isn't exist. skipping`);
  } else {
    throw parseResult.error;
  }
}

const INTERCOM_ACCESS_TOKEN = env
  .get('INTERCOM_ACCESS_TOKEN')
  .required()
  .asString();

const options = new InterComProviderOptions(INTERCOM_ACCESS_TOKEN);
const interComProvider = new InterComProvider(options);

export default interComProvider;
