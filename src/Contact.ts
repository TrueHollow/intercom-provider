import { checkResponse, NeedlePromiseFunction } from './helpers';

// https://developers.intercom.com/intercom-api-reference/reference#create-contact
export type ContractObject = {
  role: string;
  external_id: string;
  email: string;
  phone?: string;
  name?: string;
  avatar?: string;
  signed_up_at?: number;
  last_seen_at?: number;
  owner_id?: number;
  unsubscribed_from_emails?: boolean;
  custom_attributes?: any;
};

type Operator = '=' | '!=' | 'IN' | 'NIN' | '>' | '<' | '~' | '!~' | '^' | '$';

export type Query = {
  field: string;
  operator: Operator;
  value: string | Array<string> | Array<Query>;
};

export default class Contact {
  private needle: NeedlePromiseFunction;

  constructor(needle: NeedlePromiseFunction) {
    this.needle = needle;
  }

  async search(query: Query): Promise<any> {
    const data = {
      query,
    };
    const res = await this.needle(
      'post',
      'https://api.intercom.io/contacts/search',
      data
    );
    checkResponse(res);
    return res.body.data;
  }

  async update(id: string, contract: ContractObject): Promise<any> {
    const data = {
      ...contract,
    };
    const url = `https://api.intercom.io/contacts/${id}`;
    const res = await this.needle('put', url, data);
    checkResponse(res);
    return res.body;
  }

  async create(contract: ContractObject): Promise<any> {
    const data = {
      ...contract,
    };
    const url = `https://api.intercom.io/contacts`;
    const res = await this.needle('post', url, data);
    checkResponse(res);
    return res.body;
  }

  async delete(id: string): Promise<any> {
    const url = `https://api.intercom.io/contacts/${id}`;
    const res = await this.needle('delete', url, null);
    checkResponse(res);
    return res.body;
  }
}
