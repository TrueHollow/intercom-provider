import * as needleLib from 'needle';
import Bottleneck from 'bottleneck';
import InterComProviderOptions from './InterComProviderOptions';
import Contact from './Contact';
import { NeedlePromiseFunction } from './helpers';

export default class InterComProvider {
  get Contact(): Contact {
    return this._contacts;
  }
  private limiter: Bottleneck;
  private appApiKey: string;
  private needle: NeedlePromiseFunction;
  private _contacts: Contact;

  constructor(config: InterComProviderOptions) {
    this.limiter = new Bottleneck(config);
    this.appApiKey = config.appApiKey;
    const options = {
      headers: {
        Authorization: `Bearer ${this.appApiKey}`,
      },
    } as needleLib.NeedleOptions;
    needleLib.defaults(options);
    this.needle = this.limiter.wrap(needleLib);
    this._contacts = new Contact(this.needle);
  }
}
