import { NeedleResponse } from 'needle';
import * as needleLib from 'needle';

export const checkResponse = (result: NeedleResponse): void => {
  if (
    result.statusCode === undefined ||
    result.statusCode < 200 ||
    result.statusCode > 299
  ) {
    throw new Error(
      `Invalid status code: ${result.statusCode} (${JSON.stringify(
        result.body
      )})`
    );
  }
};

export type NeedlePromiseFunction = (
  method: needleLib.NeedleHttpVerbs,
  url: string,
  data: needleLib.BodyData,
  options?: needleLib.NeedleOptions
) => Promise<needleLib.NeedleResponse>;
