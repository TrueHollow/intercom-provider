// https://www.npmjs.com/package/bottleneck#refresh-interval
export default class InterComProviderOptions {
  constructor(
    appApiKey: string,
    reservoir = 165,
    reservoirRefreshAmount = 165,
    reservoirRefreshInterval = 10 * 1000,
    maxConcurrent: number | null = null,
    minTime = 0
  ) {
    this.appApiKey = appApiKey;
    this.reservoir = reservoir;
    this.reservoirRefreshAmount = reservoirRefreshAmount;
    this.reservoirRefreshInterval = reservoirRefreshInterval;
    this.maxConcurrent = maxConcurrent;
    this.minTime = minTime;
  }

  public appApiKey: string;
  public reservoir: number;
  public reservoirRefreshAmount: number;
  public reservoirRefreshInterval: number;
  public maxConcurrent: number | null;
  public minTime: number;
}
